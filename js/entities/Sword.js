Sword = function (_game, _x, _y) {
    console.log("Creating Sword!");

    this.game = _game;

    Phaser.Sprite.call(this, this.game, _x, _y, 'swordAttack');

    this.game.add.existing(this);

    this.anchor.setTo(0.5, 0.5);

    this.game.physics.arcade.enable(this);

    this.body.gravity = 0;

    this.body.setSize(32, 71, 0, 0);

    this.body.immovable = true;

    return this;
};

Sword.prototype = Object.create(Phaser.Sprite.prototype);
Sword.prototype.constructor = Sword;