//Enemy Health
var health;

Enemy = function (_game, _x, _y, target) {
    console.log("Creating Enemy!");

    this.game = _game;

    Phaser.Sprite.call(this, this.game, _x, _y, 'enemyWalking');

    this.game.add.existing(this);

    this.game.physics.arcade.enable(this);

    this.body.collideWorldBounds = true;

    this.body.setSize(30, 71, 2, 0);

    this.anchor.setTo(0.5, 0.5);

    this.target = target;

    this.MAX_SPEED = 50;

    this.MIN_DISTANCE = 32;

    return this;
};

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

//Enemies automatically chase the player from start
Enemy.prototype.update = function () {
    var distance = this.game.math.distance(this.x, this.y, this.target.x, this.target
        .y);

    if (this.target.x > this.x) {
        this.scale.x = 1;
    } else {
        this.scale.x = -1;
    }
    //if distance isnt at the player keep moving
    if (distance > enemy.MIN_DISTANCE) {
        var rotation = this.game.math.angleBetween(this.x, this.y, this.target.x,
            this.target.y);

        this.body.velocity.x = Math.cos(rotation) * enemy.MAX_SPEED;
    } else {
        enemy.body.velocity.setTo(0, 0);
    }
};