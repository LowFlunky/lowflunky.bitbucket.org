Door = function (_game, _x, _y) {
    console.log("Creating Door!");

    this.game = _game;

    Phaser.Sprite.call(this, this.game, _x, _y, 'exit');

    this.game.add.existing(this);

    this.game.physics.arcade.enable(this);

    this.body.collideWorldBounds = true;

    this.body.setSize(34, 72, 0, 0);

    this.body.gravity = 0;

    this.anchor.setTo(0.5, 0.5);

    this.game.physics.immovable = true;

    return this;
};

Door.prototype = Object.create(Phaser.Sprite.prototype);
Door.prototype.constructor = Door;