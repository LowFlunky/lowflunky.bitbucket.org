var Health;
var PotionToUse;

Player = function (_game, _x, _y) {
    console.log("Creating Player!");

    this.game = _game;

    Phaser.Sprite.call(this, this.game, _x, _y, 'playerWalking');

    this.game.add.existing(this);

    this.game.physics.arcade.enable(this);

    this.body.collideWorldBounds = true;

    this.body.setSize(30, 50, 0, 0);

    this.anchor.setTo(0.5, 1);

    Health = 10;

    PotionToUse = 1;

    pPain = this.game.add.sound('pPain');

    return this;
};

Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

//Potion picked up
Player.prototype.SetPotion = function () {
    PotionToUse = PotionToUse + 1;
};

//Potion Used
Player.prototype.UsePotion = function () {
    if (PotionToUse > 0) {
        PotionToUse--;
        Health = Health + 3;
        if (Health > 10) {
            Health = 10;
        }
    }

};

Player.prototype.Damaged = function () {
    Health = Health - 1;
    pPain.play();
};

Player.prototype.GetHealth = function () {
    return Health;
};

Player.prototype.GetPotion = function () {
    return PotionToUse;
};