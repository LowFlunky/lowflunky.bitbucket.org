Potion = function (_game, _x, _y, player) {
    console.log("Creating Potion!");

    this.game = _game;

    Phaser.Sprite.call(this, this.game, _x, _y, 'potion');

    this.game.add.existing(this);

    this.game.physics.arcade.enable(this);

    this.body.collideWorldBounds = true;

    this.body.setSize(34, 76, 2, 0);

    this.anchor.setTo(0.5, 0.5);

    this.player = player;

    return this;
};

Potion.prototype = Object.create(Phaser.Sprite.prototype);
Potion.prototype.constructor = Potion;