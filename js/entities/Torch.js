Torch = function (_game, _x, _y) {
    console.log("Creating Torches!");

    this.game = _game;

    Phaser.Sprite.call(this, this.game, _x, _y, 'torch');

    this.game.add.existing(this);

    this.anchor.setTo(0.5, 0.5);

    return this;
};

Torch.prototype = Object.create(Phaser.Sprite.prototype);
Torch.prototype.constructor = Torch;