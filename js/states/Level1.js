/*jshint enforceall: false, undef: false, unused: false, expr: false, globalstrict: false*/
BasicGame.Level1 = function(game) {
  //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

  this.map;
  this.layer;

  //  You can use any of these from any function within this State.
  //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.
};

//Variables
// Brightness variable
var brightness = 1;
// Attack boolean
var attack;
//attacking boolean
var attacking;
//attack timer
var timer;
//attack delay
var delay;
//player hurt bool
var hurt = false;
//player hurt delay timer
var hurtDelay;
// drink potion boolean
var drinkPotion = false;
// potion delay
var potionDelay;
//control variables
var jump;
var moveRight;
var moveLeft;
var attackKey;
//GUI
var GUI;
var txtHealth;
var txtPotion;
var style;
var random;
var emitter;

var hasAttacked = false;

var playing;
var door;

//sounds
var drinkingPotion;
var deathEnemy1;
var deathEnemy2;
var deathEnemy3;
var pickupItem;
var swordHitEnemy;
var swordMissEnemy;
var ambience;

// Door found for end of level
var endlevel = false;

BasicGame.Level1.prototype = {
  create: function() {
    console.log("Creating Level1 state!");

    this.physics.startSystem(Phaser.Physics.ARCADE);

    // World Background Color
    this.stage.backgroundColor = "black";
    map = this.add.tilemap('tutorial');
    map.addTilesetImage('tiles');
    map.addTilesetImage('walls');
    layer = map.createLayer('Tile Layer 1');
    layer.resizeWorld();
    map.setCollision([0, 1], true);

    // Audio
    ambience = this.game.add.sound('soundtrack');
    drinkingPotion = this.game.add.sound('drink');
    deathEnemy1 = this.game.add.sound('mDeath1');
    deathEnemy2 = this.game.add.sound('mDeath2');
    deathEnemy3 = this.game.add.sound('mDeath3');
    pickupItem = this.game.add.sound('item');
    swordHitEnemy = this.game.add.sound('swordHit');
    swordMissEnemy = this.game.add.sound('swordMiss');
    pDeath = this.game.add.sound('pDeath');

    // Start Music
    ambience.loop = true;
    ambience.volume = 0.3;
    ambience.play();

    //Exit Door
    door = new Door(this.game, 3776, 128);

    //Torches
    this.torch = this.add.group();
    this.torch.add(new Torch(this, 2496, 384));
    this.torch.add(new Torch(this, 1792, 416));
    this.torch.add(new Torch(this, 3104, 224));
    this.torch.add(new Torch(this, 288, 416));
    this.torch.add(new Torch(this, 864, 416));

    // Create Player
    player = new Player(this.game, 100, 300);
    player.animations.add('idle', [1]);
    player.animations.add('playerWalking');

    //Health Emitter
    emitter = this.game.add.emitter(this.game, 10, 10);
    emitter.makeParticles('health');
    emitter.gravity = -1;

    // Create Potion
    potion = new Potion(this.game, 200, 300, player);
    potion.scale.set(0.5, 0.5);

    // Create Enemy
    enemy = new Enemy(this.game, 300, 300, player);
    enemy1 = new Enemy(this.game, 400, 300, player);
    enemy2 = new Enemy(this.game, 600, 300, player);
    enemy3 = new Enemy(this.game, 500, 300, player);

    // Enemy animations
    enemy.animations.add('enemyWalking');
    enemy1.animations.add('enemyWalking');
    enemy2.animations.add('enemyWalking');
    enemy3.animations.add('enemyWalking');

    enemy.animations.play('enemyWalking', 20, true);
    enemy1.animations.play('enemyWalking', 20, true);
    enemy2.animations.play('enemyWalking', 20, true);
    enemy3.animations.play('enemyWalking', 20, true);

    // Create Sword
    sword = new Sword(this.game, 32, -37);
    sword.animations.add('swordAttack');
    player.addChild(sword);
    sword.visible = false;

    // Light mechanic
    this.LIGHT_RADIUS = 150;
    this.shadowTexture = this.game.add.bitmapData(
      this.world.width, this.world
      .height);
    var lightSprite = this.game.add.image(0, 0, this.shadowTexture);
    lightSprite
      .blendMode = Phaser.blendModes.MULTIPLY;

    // RayCast [WIP]
    this.bitmap = this.game.add.bitmapData(this.game.width, this.game.height);
    this.bitmap.context.fillStyle = 'rgb(255, 255, 255)';
    this.bitmap.context.strokeStyle = 'rgb(255, 255, 255)';
    var lightBitmap = this.game.add.image(0, 0, this.bitmap);

    lightBitmap.blendMode = Phaser.blendModes.MULTIPLY;

    this.rayBitmap = this.game.add.bitmapData(this.game.width, this.game.height);
    this.rayBitmapImage = this.game.add.image(0, 0, this.rayBitmap);
    this.rayBitmapImage.visible = false;

    this.game.input.onTap.add(this.toggleRays, this);

    this.game.input.activePointer.x = this.game.width / 2;
    this.game.input.activePointer.y = this.game.height / 2;

    // Physics
    this.physics.arcade.gravity.y = 500;

    //Camera
    this.camera.follow(player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT);

    //Controls
    jump = this.input.keyboard.addKey(Phaser.Keyboard.W);
    jumpSpace = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    moveRight = this.input.keyboard.addKey(Phaser.Keyboard.D);
    moveLeft = this.input.keyboard.addKey(Phaser.Keyboard.A);
    attackKey = this.input.keyboard.addKey(Phaser.Keyboard.F);
    potionKey = this.input.keyboard.addKey(Phaser.Keyboard.CONTROL);

    //Gui Text
    style = {
      font: "22px Arial",
      fill: "#ffa500",
      align: "left"
    };
    txtHealth = ("Health: " + player.GetHealth());
    txtPotion = ("Potions: " + player.GetPotion());
    GUI = this.add.text(10,
      20, txtHealth, style);
    GUI.fixedToCamera = true;

    //Attack Boolean setup
    attack = true;
    attacking = false;
  },

  //Collisions Function
  SetCollisions: function() {
    //Collisions. Functions are called when certain collisions are met.
    this.physics.arcade.overlap(sword, enemy, this.attackFunction, null,
      this);
    this.physics.arcade.overlap(sword, enemy1, this.attackFunction, null,
      this);
    this.physics.arcade.overlap(sword, enemy2, this.attackFunction, null,
      this);
    this.physics.arcade.overlap(sword, enemy3, this.attackFunction,
      null, this);
    this.physics.arcade.collide(player, door, this.nextLevelFunction, null,
      null);
    this.physics.arcade.collide(player, layer);
    this.physics.arcade.collide(potion, layer);
    this.physics.arcade.collide(player, potion, this.potionFunction, null,
      this);
    this.physics.arcade.collide(enemy, layer);
    this.physics.arcade.collide(enemy1, layer);
    this.physics.arcade.collide(enemy2, layer);
    this.physics.arcade.collide(enemy3, layer);
    this.physics.arcade.collide(player, enemy, this.hurtFunction, null,
      this);
    this.physics.arcade.collide(player, enemy1, this.hurtFunction, null,
      this);
    this.physics.arcade.collide(player, enemy2, this.hurtFunction, null,
      this);
    this.physics.arcade.collide(player, enemy3, this.hurtFunction, null,
      this);
    this.physics.arcade.collide(enemy, enemy1);
    this.physics.arcade.collide(enemy, enemy2);
    this.physics.arcade.collide(enemy, enemy3);
    this.physics.arcade.collide(enemy1, enemy2);
    this.physics.arcade.collide(enemy1, enemy3);
    this.physics.arcade.collide(enemy2, enemy3);
  },

  // Manages Shadows
  updateShadowTexture: function() {
    this.shadowTexture.context.fillStyle = 'rgba(1, 1, 1, 1.2)';
    this.shadowTexture.context.fillRect(0, 0, this.world.width, this.world.height);

    //Torches Lights
    this.torch.forEach(function(torchLight) {
      var radiusTorch = this.LIGHT_RADIUS + this.game.rnd.integerInRange(
        1, 10);
      var gradientTorch = this.shadowTexture.context.createRadialGradient(
        torchLight.x, torchLight.y, this.LIGHT_RADIUS * 0.10,
        torchLight.x,
        torchLight.y, radiusTorch);
      gradientTorch.addColorStop(0, 'rgba(226, 88, 34, 0.9)');
      gradientTorch.addColorStop(1, 'rgba(255, 88, 34, 0.0)');
      this.shadowTexture.context.beginPath();
      this.shadowTexture.context.fillStyle = gradientTorch;
      this.shadowTexture.context.arc(torchLight.x, torchLight.y, this.LIGHT_RADIUS,
        0, Math.PI * 2);
      this.shadowTexture.context.fill();
    }, this);

    //Player Light
    var radius = this.LIGHT_RADIUS + this.game.rnd.integerInRange(1, 10);
    var gradient = this.shadowTexture.context.createRadialGradient(player.body
      .x,
      player.body.y, (this.LIGHT_RADIUS * 0.45) * brightness, player.body
      .x,
      player.body.y, radius);
    gradient.addColorStop(0, 'rgba(255, 255, 255, 0.4)');
    gradient.addColorStop(1, 'rgba(255, 255, 255, 0.0)');

    this.shadowTexture.context.beginPath();
    this.shadowTexture.context.fillStyle = gradient;
    this.shadowTexture.context.arc(player.body.x, player.body.y, (this.LIGHT_RADIUS *
      1.5), 0, Math.PI * 2);
    this.shadowTexture.context.fill();
    this.shadowTexture.dirty = true;
  },

  //RayCast Testing
  toggleRays: function() {
    if (this.rayBitmapImage.visible) {
      this.rayBitmapImage.visible = false;
    } else {
      this.rayBitmapImage.visible = true;
    }
  },

  // Manages Potions
  potionFunction: function(sprite1, sprite2) {
    player.SetPotion();
    pickupItem.play();
    sprite2.kill();
    sprite2.body.x = this.rnd.between(32, this.world.width - 32);
    sprite2.body.y = this.rnd.between(32, this.world.height);
    sprite2.body.velocity.x = 0;
    sprite2.revive();
  },

  //Player Health
  hurtFunction: function(sprite1, sprite2) {
    if (hurt === false) {
      player.Damaged();
      hurtDelay = this.time.now;
      hurt = true;
    } else {
      if (this.time.now - hurtDelay > 250 && hurt === true) {
        hurt = false;
      }
    }
  },

  //If the attack function is true then the enemy being collided with will be destroyed
  attackFunction: function(sprite1, sprite2) {
    if (attacking === true) {
      sprite2.kill();
      swordHitEnemy.play();
      var x = this.rnd.between(0, 100);
      if (x >= 50) {
        var y = this.rnd.between(0, 100);
        if (y <= 33) {
          deathEnemy1.play();
        } else if (y >= 34 && y <= 66) {
          deathEnemy2.play();
        } else {
          deathEnemy3.play();
        }
      }
      sprite2.body.x = this.rnd.between(32, this.world.width - 32);
      sprite2.body.y = this.rnd.between(32, this.world.height - 71);
      sprite2.body.velocity.x = 0;
      sprite2.revive();
    }
  },

  //Set Potion count on the GUI
  SetGUI: function() {
    GUI.setText("Health: " + player.GetHealth() + "\n" + "Potions: " +
      player.GetPotion());
  },

  //Set End level bool to false
  //[DEV] - The state is undefined in this scope for some reason
  nextLevelFunction: function() {
    endlevel = true;
    //this.state.start('MainMenu');
  },

  update: function() {

    // if level is over
    if (endlevel === true) {
      ambience.stop();
      this.state.start('MainMenu');
    }

    //If players health is 0 or less then load menu
    if (player.GetHealth() === 0) {
      pDeath.play();
      this.quitGame();
    }
    //To fix resize
    layer.resizeWorld();
    //GUI function
    this.SetGUI();
    //Assign the wall of collisions
    this.SetCollisions();
    // Update shadow location per frame
    this.updateShadowTexture();
    //Drink potion
    if (potionKey.isDown && drinkPotion === false && (player.GetPotion() >
        0 && player.GetHealth() < 10)) {
      emitter.x = player.body.x;
      emitter.y = player.body.y + 10;
      emitter.start(false, 300, 100, 30);
      drinkPotion = true;
      player.UsePotion();
      potionDelay = this.time.now;
      player.body.velocity.x = 0;
      drinkingPotion.play();
    } else if (this.time.now - potionDelay > 2000 && drinkPotion === true) {
      drinkPotion = false;
    }

    //If the player isn't drinking a potion
    if (drinkPotion === false) {

      //Check if the left arrow is down.
      if (moveLeft.isDown) {
        //Give the player's physics body some velocity on the x (horizontal axis).
        player.body.velocity.x = -175;
        //Play the run animation
        player.animations.play('playerWalking', 20, true);
        player.scale.x = -1;
      }
      //Check if the right arrow is down.
      else if (moveRight.isDown) {
        //Give the player's physics body some velocity on the x (horizontal axis).
        player.body.velocity.x = 175;
        //Play the run animation
        player.animations.play('playerWalking', 20, true);
        player.scale.x = 1;
      }
      //Neither direction is pressed
      else {
        //Remove the player's physics body's horizontal velocity.
        player.body.velocity.x = 0;
        //Play the Stand animation
        player.animations.play('idle', 20, true);
      }

      //if the up arrow is pressed and mario is touching another physics body below.
      if ((jump.isDown || jumpSpace.isDown) && player.body.onFloor()) {
        //Give the player's physics body some vertical velocity.
        player.body.velocity.y = -350;
      }

      // Attack function to handle timers per attack and enable/disable the sword
      if (attack === true && this.game.input.activePointer.isDown) {
        attack = false;
        attacking = true;
        delay = this.time.now + 1000;
        timer = this.time.now + 450;
        swordMissEnemy.play();
        sword.visible = true;
        playing = sword.animations.play('swordAttack', 20);
      }

      if (attacking === true && this.time.now > delay) {
        attacking = false;
        attack = true;
        sword.visible = false;
      }

      if (attacking === true && this.time.now > timer) {
        sword.visible = false;
      }
    }
  },

  //Render to show FPS for debugging
  render: function() {
    this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");
  },

  quitGame: function(pointer) {
    //  Here you should destroy anything you no longer need.
    //  Stop music, delete sprites, purge caches, free resources, all that good stuff.
    //  Then let's go back to the main menu.
    this.state.start('MainMenu');
  }

};
