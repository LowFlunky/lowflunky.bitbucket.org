BasicGame.Preloader = function(game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function() {

		//	These are the assets we loaded in Boot.js
		//	A nice sparkly background and a loading progress bar

		this.background = this.add.tileSprite(0, 0, 1024, 860,
			'preloaderBackground');
		this.preloadBar = this.add.sprite(300, 400, 'preloaderBar');

		// Dun
		this.backgroundColor = 0xffffff;

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.

		this.load.setPreloadSprite(this.preloadBar);

		this.time.advancedTiming = true;
		//	Here we load the rest of the assets our game needs.
		//	You can find all of these assets in the Phaser Examples repository

		// e.g. this.load.image('image-name', 'assets/sprites/sprite.png');

		//Menu World
		this.load.tilemap('menu', 'assets/Menu.json', null, Phaser.Tilemap.TILED_JSON);
		//Menu Objects
		this.load.image('startButton', 'assets/Start.png');

		// Prototype/Survival World
		this.load.tilemap('level', 'assets/Prototype.json', null, Phaser.Tilemap
			.TILED_JSON);
		this.load.image('tiles', 'assets/tileable_bricks_single.jpg');
		this.load.image('walls', 'assets/wall.jpg');

		//Tutorial World
		this.load.tilemap('tutorial', 'assets/Tutorial.json', null, Phaser.Tilemap
			.TILED_JSON);
		this.load.image('tiles', 'assets/tileable_bricks_single.jpg');
		this.load.image('walls', 'assets/wall.jpg');


		// Game Objects
		this.load.image('potion', 'assets/potion.png');
		this.load.image('brick', 'assets/brick.png');
		this.load.image('enemy', 'assets/Enemy.png');
		this.load.image('player', 'assets/player.png');
		this.load.image('torch', 'assets/Torch.png');
		this.load.image('sword', 'assets/Sword.png');
		this.load.image('health', 'assets/HealthSprite2.png');
		this.load.image('exit', 'assets/door.png');

		//Animations
		this.load.spritesheet('playerWalking', 'assets/walking.png', 30,
			71, 2);
		this.load.spritesheet('enemyWalking', 'assets/walkingEnemy.png', 30,
			71, 2);
		this.load.spritesheet('swordAttack', 'assets/swordAttack.png', 32,
			71, 4);

		// Audio
		this.load.audio('swordHit', 'assets/audio/Sword Hit V1.wav');
		this.load.audio('swordMiss', 'assets/audio/Sweeping Sword V1.wav');
		this.load.audio('item', 'assets/audio/Hidden Item V1.wav');
		this.load.audio('mDeath1', 'assets/audio/Monster Dies 01 V1.wav');
		this.load.audio('mDeath2', 'assets/audio/Monster Dies 02 V1.wav');
		this.load.audio('mDeath3', 'assets/audio/Monster Dies 03 V1.wav');
		this.load.audio('drink', 'assets/audio/Potion Relief V1.ogg');
		this.load.audio('pDeath', 'assets/audio/Player Death.wav');
		this.load.audio('pPain', 'assets/audio/Player Pain.wav');

		//Music - https://www.freesound.org/people/FoolBoyMedia/sounds/237268
		this.load.audio('soundtrack', 'assets/audio/placeholder.mp3');
	},


	create: function() {

		console.log("Creating preload state!");

		this.state.start('MainMenu');

	}

};
