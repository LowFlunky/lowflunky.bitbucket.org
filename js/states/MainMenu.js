/*jshint enforceall: false, undef: false, unused: false, expr: false, globalstrict: false*/
BasicGame.MainMenu = function (game) {
    this.add;
    this.map;
    this.layer;

};

//Variables
var start;
var GUI;
var txtTutorial;
var style;

BasicGame.MainMenu.prototype = {

    create: function () {

        console.log("Creating menu state!");

        this.stage.backgroundColor = "black";
        map = this.add.tilemap('menu');
        map.addTilesetImage('walls');
        layer = map.createLayer('startMenu');
        layer.resizeWorld();

        start = this.game.add.sprite(this.world.centerX + 20, this.world.centerY,
            'startButton');
        start.anchor.set(0.5);
        start.tint = "#ff3030";

        //Torches
        this.torch = this.add.group();
        this.torch.add(new Torch(this, 400, 250));
        this.torch.add(new Torch(this, 600, 250));

        //Menu pointer light
        this.movingLight = new Torch(this.game, this.game.width / 2, this.game.height /
            2);
        this.torch.add(this.movingLight);

        this.game.input.activePointer.x = this.game.width / 2;
        this.game.input.activePointer.y = this.game.height / 2;

        // Light mechanic
        this.LIGHT_RADIUS = 200;
        this.shadowTexture = this.game.add.bitmapData(this.world.width, this.world
            .height);
        var lightSprite = this.game.add.image(0, 0, this.shadowTexture);
        lightSprite.blendMode = Phaser.blendModes.MULTIPLY;

        // Tutorial GUI
        style = {
            font: "22px Arial",
            fill: "#ffa500",
            align: "left"
        };
        txtTutorial = (
            "W/A/S/D = Movement\nLeft Click = Attack\nLeft CTRL = Use Potion\nFind the doors and ESCAPE!"
        );
        GUI = this.add.text(10, 20, txtTutorial, style);
        GUI.setText;

        // Add some buttons
    },

    startGame: function () {
        this.state.start('Tutorial');
    },

    update: function () {
        //Start Click
        start.anchor.set(0.5);
        start.inputEnabled = true;

        start.events.onInputDown.add(this.startGame, this);


        //Pointer light
        this.movingLight.x = this.game.input.activePointer.x;
        this.movingLight.y = this.game.input.activePointer.y;

        //Shadow renderer
        this.shadowTexture.context.fillStyle = 'rgba(1, 1, 1, 1.2)';
        this.shadowTexture.context.fillRect(0, 0, this.world.width, this.world.height);

        //Torches Lights
        this.torch.forEach(function (torchLight) {
            var radiusTorch = this.LIGHT_RADIUS + this.game.rnd.integerInRange(
                1, 10);
            var gradientTorch = this.shadowTexture.context.createRadialGradient(
                torchLight.x, torchLight.y, this.LIGHT_RADIUS * 0.10,
                torchLight.x,
                torchLight.y, radiusTorch);
            gradientTorch.addColorStop(0, 'rgba(226, 88, 34, 1.0)');
            gradientTorch.addColorStop(1, 'rgba(255, 88, 34, 0.0)');
            this.shadowTexture.context.beginPath();
            this.shadowTexture.context.fillStyle = gradientTorch;
            this.shadowTexture.context.arc(torchLight.x, torchLight.y, this.LIGHT_RADIUS,
                0, Math.PI * 2);
            this.shadowTexture.context.fill();
        }, this);
        this.shadowTexture.dirty = true;

    },

    resize: function (width, height) {

        //	If the game container is resized this function will be called automatically.
        //	You can use it to align sprites that should be fixed in place and other responsive display things.

    }

};